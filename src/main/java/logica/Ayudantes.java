
package logica;

import java.util.ArrayList;
import java.util.List;


public class Ayudantes {
    private List<Alumno> ayudantes;

    public Ayudantes() {
        ayudantes=new ArrayList<>();
    }
    
    public boolean agregarAyudantes(Alumno a){
        return ayudantes.add(a);
    }

    public List<Alumno> getAyudantes() {
        return ayudantes;
    }

    public void setAyudantes(List<Alumno> ayudantes) {
        this.ayudantes = ayudantes;
    }

    
    
    
}
