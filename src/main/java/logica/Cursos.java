
package logica;

import java.util.List;


public class Cursos {
    private List<Curso> cursos;

    public Cursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

}
