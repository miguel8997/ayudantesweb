
package logica;

public class Curso {
    private String ramo;

    public Curso(String ramo) {
        this.ramo = ramo;
    }

    public String getRamo() {
        return ramo;
    }

    public void setRamo(String ramo) {
        this.ramo = ramo;
    }
    
    
}
