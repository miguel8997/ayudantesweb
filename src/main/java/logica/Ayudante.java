
package logica;


public class Ayudante extends Alumno {
    private Ayudantia ayudantia;

    public Ayudante(Ayudantia ayudantia,String nombre, String carrera) {
        super(nombre, carrera);
        this.ayudantia=ayudantia;
    }

    public Ayudantia getAyudantia() {
        return ayudantia;
    }

    public void setAyudantia(Ayudantia ayudantia) {
        this.ayudantia = ayudantia;
    }

}
