
package logica;

public class Estudiante extends Alumno {
    private int puntos;
    
    public Estudiante(String nombre, String carrera,int puntos) {
        super(nombre, carrera);
        this.puntos=puntos;
    }
    
    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    
    
}
