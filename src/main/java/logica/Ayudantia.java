package logica;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Miguel Ortiz
 */
public class Ayudantia {
    private String carrera;
    private String ramo;
    private String fecha;
    private String tema;
    private List<Alumno> alumnosInscritos;

    public Ayudantia() {
        alumnosInscritos=new ArrayList<>();
    }
    
    public boolean agregarAlumno(Alumno a){
        return alumnosInscritos.add(a);
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getRamo() {
        return ramo;
    }

    public void setRamo(String ramo) {
        this.ramo = ramo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public List<Alumno> getAlumnosInscritos() {
        return alumnosInscritos;
    }

    public void setAlumnosInscritos(List<Alumno> alumnosInscritos) {
        this.alumnosInscritos = alumnosInscritos;
    }
    
    
}
